---
title: An introduction to Cryptocurrencies
subtitle: A well-designed tale
date: 2017-11-06
---

This is a 101 on cryptocurrency in general. If you need a high level explanation, or just an refresher, feel free to read this post. This post assumes the reader has no knowledge of cryptocurrency whatsoever.

<!--more-->

# What is cryptocurrency?

There are many explainations that exist that define "Cryptocurrency": Wikipedia defines it as "A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange using cryptography to secure the transactions, to control the creation of additional units, and to verify the transfer of assets."[^wikidef]. [Others][blockgeeks] don't even explain it at all; instead, they just give a brief history, then jumping into blockchains, consensus, nodes and what not.

Lets just break "cryptocurrency" apart, literally:

"Cypto-", the prefix shorthand for "cryptography", means something related to keeping data hidden from others.

"currency" is a system of money.

Thus, cryptocurrency is simply "a system of money based on keeping data hidden from others." In other words, it's money based on the equations and formulas that keep your data safe and make it impossible for malicious people to read your online banking data, online government information, or even emails. All cryptocurrencies have some basis of cryptography build into their currency exchanges, making it a fundamental part of the currency itself. But like the word itself, we can perhaps break down the process into two parts, the cryptography, and the and currency.

## Currency
It's easier to first explain the currency. Like all fiat currencies, or currencies that are backed by a government, cryptocurrencies obtain their value by the trust of the users. However, unlike fiat currencies, which users put their trust into the government, users put trust into the cryptography behind the coin itself. Since there's no government, and you can't place a concept at a certain location, we often say that there is no centralization&emdash;**or rather, decentralization**&emdash;in these currencies. That means, no matter how many places shut down, so long as there's a way to access the process the algorithm, you can use the currency.

Despite this profound difference, users are free to exchange the cryptocurrency and fiat currencies like any other currency, and the value of the cryptocurrency is often determined by this. In fact, most new currencies' prices are determined by the rate people are willing to trade for existing currencies, not just cryptocurrencies. Then, users are free to sell the cryptocurrency or use it and exchange it for goods or services, like any other currency. If you never use any advanced feature of a cryptocurrency, they can essentially be treated as another currency.

## Cryptography

### The Blockchain
First, imagine a infinitely long list that people can write what they buy or sell on. At each new line is a number, slowing decreasing over the length of the ledger. People trust in this list, and that anything that's written onto this list is taken as absolute truth, that an exchange has definitely happened. However, to write to this list, you need to guess a number that's lower than the number at that line. Problem is, you don't choose what number you guess. A machine guesses it for you.

You could spend all day trying to have the machine guess it for you, or you can ask a person to guess for you, and if they guess a correct number, they can add both yours and their transactions to the list. In return, you'd pay them a small fee. Now, there exist two types of people: the people who make transactions, and the people who get the machine to generate a number that's lower than the number at the current line in the list. When someone guesses a good number, everyone else takes a good look at the number and makes sure it's valid. If mostly everyone agrees, then people let the person add it to the list.

What I've just said was a nearly one-to-one analogy to the blockchain. Instead of a infinitely long list, you have a *blockchain*. The people who run the machines to guess the number are *miners*, and the people who ask them to put their transactions into the same one as the miner's are the people that use the cryptocurrency. The fee that they pay to add their transaction into the miner's group of transactions is called the *transaction fee*. The group of transactions are called a *block*. The number that slowly decreases is called the *difficulty*, and the effectively-random number that's generated is called the *hashing function*.

All cryptocurrencies effectively run off this system. Users pay a small *transaction fee* to put their transactions into a *block*, where miners then try to turn their block into a humber using the *hashing function*. If that number is lower than the *difficulty*, then that block is added to the *blockchain*, if and only if a certain percentage of everyone else agrees that that number is valid&emdash;*consensus*.

While many implement their hashing function or consensus differently, the general concepts apply to all cryptocurrencies as a inherit result of their crytographic nature.

### The hashing function
The hashing function is what actually turn a currency into a crytocurrency. In short, it effectively turns an arbitrary piece of data into a number. It's impossible to extract the original data from the number due to how large the number is, yet due to the size of the number, every combination of data creates an effectively unique number. This means that when given both the original data and the output, people can verify that this data yielded this number.

### Privacy
Wait, if everything is on the blockchain, won't people know if I purchased some obscene item for x amount? Yes and no. While people can see how much was exchanged, it's nearly impossible to know why. This is because people can create an abitrary amount of *addresses*, strings of unique characters that act as a "mail to" address for cryptocurrency. If they want to make a purchase, simply make a new address and add it to your *wallet*, a keychain of addresses. This way, people effectively, to some degree, anonymity.

## Conclusion
These were all the basic concepts presented in nearly all cryptocurrencies. Understanding these are key to understanding the myriad of cryptocurrencies in the world.

[^wikidef]: https://en.wikipedia.org/wiki/Cryptocurrency
[blockgeeks]: https://blockgeeks.com/guides/what-is-cryptocurrency/
